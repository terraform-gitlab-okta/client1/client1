# Terraform GitLab OKTA Configuration #
### Okta QAS is tagged to Qas Branch in Gitlab and Okta Production is tagged to production branch.
 ## Components -
 * .gitlab-ci.yml - GitLab CI File that contains the information about the GitLab Pipeline

 * backend.tf - Contains the configuration about the remote Azure Backend. This backend is a Blob Storage used to main state file for all environments.

 * main.tf - Contains the configuration file, basically all the terraform resources are stored here.

 * provider.tf - Configuration for the Okta Terraform provider.

 * variables.tf - Contains the default data type and value for the variables



 ## Required Enivronment Variables - 
 `ARM_ACCESS_KEY` - Contains the access key for Azure Storage account.

 `QAS_Access_Token` - API Token from Okta QAS Environment

 `PROD_Access_Token` - API Token from the Okta Production Environment.

 `QAS_Domain` - Organization name for the Okta QAS Instance. e.g. dev-12345

 `PROD_Domain` - Organization name for the Okta Production Instance.

 #### All the environment variables mentioned above are case sensistive.

 ## Procedure to create new Okta Resources -
 * Create the resource configuration in main.tf file.
 * Commit and push the changes to Qas branch. Pushing to Production will not work as Production is a protected branch.
 * Once changes are pushed to the Qas Branch it will automatically trigger the pipeline to create resources in Okta QAS.
 * Upon successful completion of Qas Pipeline create a merge request with the Production Domain, upon approval and merge automatically Pipeline will run in Production branch to create the resources in Production Instance. 

