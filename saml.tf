module "GitlabDemoApp111" {
  source = "./Modules/SAML"
  SAMLAppLabel = {
    qas        = "Salesforce-qas"
    production = "Salesforce-prod"
  }
  SAMLSSOUrl = {
    qas        = "https://tcs-511-dev-ed.my.salesforce.com"
    production = "https://tcs-511-dev-ed.my.salesforce.com"
  }
  SAMLrecipient = {
    qas        = "https://tcs-511-dev-ed.my.salesforce.com"
    production = "https://tcs-511-dev-ed.my.salesforce.com"
  }
  SAMLdestination = {
    qas        = "https://tcs-511-dev-ed.my.salesforce.com"
    production = "https://tcs-511-dev-ed.my.salesforce.com"
  }
  SAMLaudience = {
    qas        = "https://tcs-511-dev-ed.my.salesforce.com"
    production = "https://tcs-511-dev-ed.my.salesforce.com"
  }
  SAMLauthn_context_class_ref = {
    qas        = "urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport"
    production = "urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport"
  }
  SAMLAppGroups = ["SalesforceAdmin", "SalesforceUsers"]
  Attributes = {
    "firstName" = "user.firstName",
    "lastName"  = "user.lastName"
  }
}
output "GitLabDemoApp111_meta" {
  value = module.GitlabDemoApp111.Okta_metadata
}