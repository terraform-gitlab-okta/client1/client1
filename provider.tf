terraform {
  required_providers {
    okta = {
      source  = "oktadeveloper/okta"
      version = "3.10.0"
    }
  }
}

# Configure the Okta Provider
provider "okta" {
  org_name  = var.OrgName
  base_url  = "okta.com"
  api_token = var.access_api_token
}