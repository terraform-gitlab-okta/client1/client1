resource "okta_app_saml" "GitLabApp"{

    lifecycle {
        ignore_changes = [groups]
    }

    label = var.SAMLAppLabel[terraform.workspace]
    sso_url = var.SAMLSSOUrl[terraform.workspace]
    recipient = var.SAMLrecipient[terraform.workspace]
    destination = var.SAMLdestination[terraform.workspace]
    audience = var.SAMLaudience[terraform.workspace]
    signature_algorithm = "RSA_SHA256"
    digest_algorithm = "SHA256"
    authn_context_class_ref = var.SAMLauthn_context_class_ref[terraform.workspace]
    subject_name_id_template = "$${user.userName}"
    subject_name_id_format = "urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress"
    response_signed = true
    attribute_statements {
        name = "firstName"
        values = ["user.firstName"]
    }

}

resource "okta_app_group_assignment" "example" {
    for_each = toset(var.SAMLAppGroups)
    app_id = okta_app_saml.GitLabApp.id
    group_id = data.okta_group.SAMLGroup[each.key].id

}

output "Okta_metadata"{
    value = okta_app_saml.GitLabApp.metadata_url
}