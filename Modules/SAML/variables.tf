variable "SAMLAppLabel"{}
variable "SAMLSSOUrl"{}
variable "SAMLrecipient" {}
variable "SAMLdestination" {}
variable "SAMLaudience" {}
variable "SAMLauthn_context_class_ref" {}
variable "SAMLAppGroups"{
    default = [""]
}
variable "Attributes"{}