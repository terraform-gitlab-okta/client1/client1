terraform{
    required_providers{
        okta= {
            source = "oktadeveloper/okta"
            version = "3.10.0"
    }
    }
}